## Unity concepts learned in this project
* Use Transform.Translate and Transform.Rotate to move and rotate game object .
* Customize input parameter by [SerializeField] .
* Control game object manually by Input.GetAxis .
* Time.deltaTime .
* Add colliders and rigidbodies component in game objects and use OnCollisionEnter2D and OnTriggerEnter2D to listen collision and trigger events.
* Add tags to game objects to distinguish types of object .
* Simple following camera based on game object's position .
* Destroy game objects with delay time .
* Use GetComponent to access game object's components.
